//
//  MovieInfoDetailsViewController.swift
//  GetMyMovie
//
//  Created by Sandesh on 15/04/17.
//  Copyright © 2017 Mindtree Ltd. All rights reserved.
//

import UIKit

class MovieInfoDetailsViewController: UIViewController {
    
    var movieInfo: MovieInfo?
    @IBOutlet weak var movieInfoImage: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var descTextView: UITextView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var voteAvgLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        movieInfoImage.kf.setImage(with: URL(string: (movieInfo?.movieBackdropPath)!))
        titleLabel.text = movieInfo?.movieTitle
        yearLabel.text = movieInfo?.movieYear
        voteAvgLabel.text = movieInfo?.movieVoteAvg
        descTextView.text = movieInfo?.movieDesc
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
