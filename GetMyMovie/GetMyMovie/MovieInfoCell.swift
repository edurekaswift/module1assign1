//
//  MovieInfoCell.swift
//  GetMyMovie
//
//  Created by Sandesh on 15/04/17.
//  Copyright © 2017 Mindtree Ltd. All rights reserved.
//

import UIKit

class MovieInfoCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
}
