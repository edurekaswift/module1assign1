//
//  ViewController.swift
//  GetMyMovie
//
//  Created by Sandesh on 15/04/17.
//  Copyright © 2017 Mindtree Ltd. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftyJSON

struct MovieInfo {
    var movieTitle: String
    var movieVoteAvg: String
    var moviePosterPath: String
    var movieBackdropPath: String
    var movieDesc: String
    var movieId: String
    var movieYear: String
}

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    let insets = UIEdgeInsets(top: 20.0, left: 10.0, bottom: 20.0, right: 10.0)
    let itemsPerRow:CGFloat = 3.0
    var totalPageCount = 1;
    var currentPageNo = 1;
    
    @IBOutlet weak var movieInfoCollectionView: UICollectionView!
    @IBOutlet weak var refreshBarButton: UIBarButtonItem!
    
    var movieInfos = [MovieInfo]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        movieInfoCollectionView.dataSource = self
        movieInfoCollectionView.delegate = self
        self.navigationController?.navigationBar.backgroundColor = UIColor.black
        getMovieDetails(pageNo: "\(1)")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getMovieDetails(pageNo:String) {
        Alamofire.request("\(MOVIE_URL)"+"\(pageNo)", method: .get).responseJSON { response in
            if let data = response.data {
                self.parseMovieData(data: data)
                self.movieInfoCollectionView.reloadData()
            }
        }
    }
    
    func parseMovieData(data:Data) {
        let json = SwiftyJSON.JSON(data: data)
        let results = json["results"].arrayValue
        
        for result in results {
            let title = result["title"].stringValue
            let voteAvg = "\(result["vote_average"].floatValue)"
            let posterPath = "\(IMAGE_URL_PREFIX)\(result["poster_path"].stringValue)"
            let backdropPath = "\(IMAGE_URL_PREFIX)\(result["backdrop_path"].stringValue)"
            let desc = result["overview"].stringValue
            let id = "\(result["id"].intValue)"
            let year = "\(result["release_date"])"
            totalPageCount = json["total_pages"].intValue
            
            movieInfos.append(MovieInfo(movieTitle: title, movieVoteAvg: voteAvg, moviePosterPath: posterPath, movieBackdropPath:backdropPath, movieDesc:desc, movieId:id, movieYear:year))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieInfos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let movieInfoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MovieInfoCell
        let movieInfo = movieInfos[indexPath.row]
        let movieUrl = URL(string: movieInfo.moviePosterPath)
        movieInfoCell.movieImage.kf.setImage(with: movieUrl)
        
        movieInfoCell.backgroundColor = UIColor.gray
        
        // add a border
        movieInfoCell.layer.borderColor = UIColor.lightGray.cgColor
        movieInfoCell.layer.borderWidth = 2
        movieInfoCell.layer.cornerRadius = 5 // optional

        return movieInfoCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth = self.view.frame.width
        let paddingSpace = (itemsPerRow + 1) * insets.left
        let availableWidth = screenWidth - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        let heightPerItem = availableWidth / itemsPerRow * 1.54
        
        return CGSize(width: widthPerItem, height: heightPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return insets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return insets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellIndex = indexPath.row
        let movieInfo = movieInfos[cellIndex]
        
        let movieInfoDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "movieInfoDetailsVCId") as! MovieInfoDetailsViewController
        movieInfoDetailVC.movieInfo = movieInfo
        self.navigationController?.pushViewController(movieInfoDetailVC, animated: true)
    }
    
    @IBAction func refreshMovieInfo(_ sender: UIBarButtonItem) {
        movieInfos.removeAll()
        currentPageNo = 1;
        getMovieDetails(pageNo: "\(1)")
        movieInfoCollectionView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    
    @IBAction func loadMoreMovieInfo(_ sender: UIBarButtonItem) {
        
        if currentPageNo < totalPageCount {
            currentPageNo += 1
            getMovieDetails(pageNo: "\(currentPageNo)")
        }
        
    }
    
}
