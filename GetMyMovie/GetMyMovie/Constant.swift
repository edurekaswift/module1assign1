//
//  Constant.swift
//  GetMyMovie
//
//  Created by Sandesh on 17/04/17.
//  Copyright © 2017 Mindtree Ltd. All rights reserved.
//

import Foundation

let MOVIE_URL = "https://api.themoviedb.org/3/movie/now_playing?api_key=17ed04d9efe959364d760390eb044a27&language=en-US&page="

let IMAGE_URL_PREFIX = "https://image.tmdb.org/t/p/w780"
