//
//  ViewController.swift
//  Module1Assign1
//
//  Created by Sandesh on 04/03/17.
//  Copyright © 2017 Mindtree Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var intSlider: UISlider!
    @IBOutlet weak var intLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func intUpdateAction(_ sender: UISlider) {
        intLabel.text = "\(Int(sender.value))"
        let intValue = Int(sender.value)
        
        switch intValue {
        case 0...3:
            intLabel.backgroundColor = UIColor.red
            
        case 4...7:
            intLabel.backgroundColor = UIColor.green
            
        case 8...10:
            intLabel.backgroundColor = UIColor.blue
            
        default:
            intLabel.backgroundColor = UIColor.gray
        }
        
    }
}

